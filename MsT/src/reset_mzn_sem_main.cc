#include <iostream>
#include <cstdio>

#include "RegisterMapCommon.hh"
#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "FPGAModule.hh"
#include "MifFunc.hh"
#include "rbcp.hh"

using namespace HUL;
void set_error_injection_address(HUL::FPGAModule& fpga_module, uint32_t addr_base,
				 uint32_t command,
				 uint32_t addr_lniear_frame,
				 uint32_t addr_word,
				 uint32_t addr_bit
				 );

int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "read_mzn_sem [IP address]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);

  printf("==== Mezzanine slot Down ====\n");

  printf("Reset SEM controller\n");
  set_error_injection_address(fpga_module, MsT::MIF::kDown,
			      SDS::kSetIdle, 0, 0, 0);
  WriteMIFModule(fpga_module, MsT::MIF::kDown, HRTDC_MZN::SDS::kAddrSemErrStrobe, 1, 1);

  sleep(1);
  
  set_error_injection_address(fpga_module, MsT::MIF::kDown,
			      SDS::kSetReset, 0, 0, 0);
  WriteMIFModule(fpga_module, MsT::MIF::kDown, HRTDC_MZN::SDS::kAddrSemErrStrobe, 1, 1);

  return 0;

}// main

// control sem in mzn ___________________________________________________________________
void
set_error_injection_address(HUL::FPGAModule& fpga_module, uint32_t addr_base,
			    uint32_t command,
			    uint32_t addr_linear_frame,
			    uint32_t addr_word,
			    uint32_t addr_bit
			    )
{
  // command              :  4bit
  // linear frame address : 17bit
  // word address         :  7bit
  // bit address          :  5bit
  const uint32_t mask_linear = 0x1ffff;
  const uint32_t mask_word   = 0x7f;
  const uint32_t mask_bit    = 0x1f;

  const uint32_t shift_word   = 5;
  const uint32_t shift_linear = 7 + shift_word;

  // For command (5th byte)
  const uint32_t mask_command  = 0xf;
  const uint32_t shift_command = 4;

  uint32_t reg_1st_4th = addr_bit & mask_bit;
  reg_1st_4th = reg_1st_4th | ((addr_word & mask_word) << shift_word);
  reg_1st_4th = reg_1st_4th | ((addr_linear_frame & mask_linear) << shift_linear);
  WriteMIFModule(fpga_module, addr_base,
		 HRTDC_MZN::SDS::kAddrSemErrAddr, reg_1st_4th, 4);

  uint32_t reg_5th = (command & mask_command) << shift_command;
  WriteMIFModule(fpga_module, addr_base,
		 HRTDC_MZN::SDS::kAddrSemErrAddr+4, reg_5th, 1);
}
