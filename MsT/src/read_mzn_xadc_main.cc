#include <iostream>
#include <cstdio>

#include "RegisterMapCommon.hh"
#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "FPGAModule.hh"
#include "MifFunc.hh"
#include "rbcp.hh"

using namespace HUL;
void read_a_mzn(HUL::FPGAModule& fpga_module, uint32_t addr_base);

int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "read_mzn_xadc [IP address]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);

  printf("==== Mezzanine slot Down ====\n");
  read_a_mzn(fpga_module, MsT::MIF::kDown);

  return 0;

}// main

// read_a_mzn ____________________________________________________________________________
void
read_a_mzn(HUL::FPGAModule& fpga_module, uint32_t addr_base)
{
  // XADC _____________________________________________________________
  WriteMIFModule(fpga_module, addr_base,
		 HRTDC_MZN::SDS::kAddrXadcDrpMode, SDS::kDrpReadMode, 1);

  // Read temperature monitor
  WriteMIFModule(fpga_module, addr_base,
		 HRTDC_MZN::SDS::kAddrXadcDrpAddr, SDS::kAddrDrpTemp, 1);
  WriteMIFModule(fpga_module, addr_base,
		 HRTDC_MZN::SDS::kAddrXadcExecute, 1, 1);
  uint32_t adc_temp = ReadMIFModule(fpga_module, addr_base,
				    HRTDC_MZN::SDS::kAddrXadcDrpDout, 2);

  // Read VCCINT
  WriteMIFModule(fpga_module, addr_base,
		 HRTDC_MZN::SDS::kAddrXadcDrpAddr, SDS::kAddrDrpVccInt, 1);
  WriteMIFModule(fpga_module, addr_base,
		 HRTDC_MZN::SDS::kAddrXadcExecute, 1, 1);
  uint32_t adc_vccint = ReadMIFModule(fpga_module, addr_base,
				      HRTDC_MZN::SDS::kAddrXadcDrpDout, 2);

  // Read VCCAUX
  WriteMIFModule(fpga_module, addr_base,
		 HRTDC_MZN::SDS::kAddrXadcDrpAddr, SDS::kAddrDrpVccAux, 1);
  WriteMIFModule(fpga_module, addr_base,
		 HRTDC_MZN::SDS::kAddrXadcExecute, 1, 1);
  uint32_t adc_vccaux = ReadMIFModule(fpga_module, addr_base,
				      HRTDC_MZN::SDS::kAddrXadcDrpDout, 2);
  
  // Read status
  uint32_t status = ReadMIFModule(fpga_module, addr_base,
				  HRTDC_MZN::SDS::kAddrSdsStatus, 1);
  
  // translate
  const uint32_t shift   = 4;
  //  const uint32_t mask    = 0xfff;
  const uint32_t max_adc = 0x1000;

  double temp = (adc_temp >> shift)*503.975/max_adc - 273.15; // 503.975: magic number
  double vccint = 3.0*(adc_vccint >> shift)/max_adc;
  double vccaux = 3.0*(adc_vccaux >> shift)/max_adc;

  printf("FPGA temp.  : %.2f C\n", temp);
  printf("VCCINT      : %.4f V\n", vccint);
  printf("VCCAUX      : %.4f V\n", vccaux);
  printf("\n");

  // Status
  printf("XADC status (0:false, 1:true)\n");
  printf(" - Over temp.   (Th. 125C)   : %d\n", (status & SDS::kXadcOverTemperature) >> 0);
  printf(" - Temp alarm   (Th. 85C)    : %d\n", (status & SDS::kXadcUserTempAlarm)   >> 1);
  printf(" - VCCINT alarm (0.97-1.03V) : %d\n", (status & SDS::kXadcUserVccIntAlarm) >> 2);

}
