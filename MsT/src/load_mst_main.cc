#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdio>

#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "MifFunc.hh"
#include "FPGAModule.hh"

const int32_t kNumTOF = 32;
const int32_t kNumSCH  = 64;
const int32_t kNumMstArray = kNumTOF*kNumSCH;
uint32_t registers_low[kNumMstArray] = {0};
uint32_t registers_high[kNumMstArray] = {0};

enum ArgIndex{kExec, kIp, kMstParam, kNumArgIndex};

int32_t read_mst_file(std::string& filename);
void    send_mst_parameter(std::string& ip);

int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "load_mst [IP address] [mst file]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[kIp];
  std::string file_name = argv[kMstParam];
  if(-1 == read_mst_file(file_name)) return -1;
  send_mst_parameter(board_ip);
  
  std::cout << "#D : The MsT paramter (" << file_name << ") is successfully sent" << std::endl;
  std::cout << "MsT:   " << file_name << std::endl;

  return 0;

}// main

int32_t
read_mst_file(std::string& filename)
{
  std::ifstream ifs(filename.c_str());
  if(!ifs.is_open()){
    std::cout << "#E : No such the mst file" << std::endl;
    return -1;
  }

  int32_t index = 0;
  std::string line;
  while(getline(ifs, line)){
    // this is comment line
    if(line[0] == '#' || line.empty()) continue;
    
    std::istringstream LineToWord(line);
    std::string word;
    uint32_t reg_low;
    uint32_t reg_high;
    // skip CH#
    LineToWord >> word;
    LineToWord >> reg_low; // this is low
    LineToWord >> reg_high; // this is high
    //    std::cout << reg_low << " " << reg_high << std::endl;

    registers_low[index]  = reg_low;
    registers_high[index] = reg_high;
    //    std::cout << registers[index] << std::endl;
    
    ++index;
  }// while

  return 0;
}// read_matrix_file

void
send_mst_parameter(std::string& ip)
{
  using namespace HUL;

  RBCP::UDPRBCP udp_rbcp(ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);

  for(int32_t i = kNumMstArray-1; i>-1; --i){
    uint32_t reg_low  = registers_low[i];
    uint32_t reg_high = registers_high[i];
    fpga_module.WriteModule(MsT::MST::kAddrWinMin,  reg_low,  3);
    fpga_module.WriteModule(MsT::MST::kAddrWinMax,  reg_high, 3);
    fpga_module.WriteModule(MsT::MST::kAddrExec, 0);
    //    std::cout << reg_high << " " << reg_low<< std::endl;
  }// for(i)
}// send_matrix
