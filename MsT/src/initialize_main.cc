#include <iostream>
#include <iomanip>
#include <ios>
#include <cstdio>

#include "RegisterMap.hh"
#include "network.hh"
#include "FPGAModule.hh"
#include "UDPRBCP.hh"
#include "MifFunc.hh"
#include "rbcp.hh"
#include "DaqFuncs.hh"

using namespace HUL;
int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "initialize [IP address]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);
  fpga_module.WriteModule(BCT::kAddrReset, 0);

  fpga_module.WriteModule(MsT::MIF::kDown + MsT::MIF::kAddrForceReset, 1);
  fpga_module.WriteModule(MsT::MIF::kDown + MsT::MIF::kAddrForceReset, 0);
  
  DdrInitialize(fpga_module);
  CalibLUT(fpga_module, MsT::MIF::kDown);


  return 0;

}// main
