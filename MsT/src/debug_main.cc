#include <iostream>
#include <iomanip>
#include <ios>
#include <cstdio>

#include "RegisterMap.hh"
#include "network.hh"
#include "FPGAModule.hh"
#include "UDPRBCP.hh"
#include "MifFunc.hh"
#include "rbcp.hh"

using namespace HUL;
int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "debug [IP address]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);
  uint32_t bct_version = fpga_module.ReadModule(BCT::kAddrVersion, 4);
  uint32_t major_version = (bct_version >> 8) & 0xff;
  uint32_t minor_version = (bct_version) & 0xff;

  std::cout << "#D: HUL firmware" << std::endl;
  std::cout << std::hex;
  std::cout << std::setfill('0') << std::right 
	    << "FW ID      : 0x" << std::setw(4) << ((bct_version >> 16) & 0xffff) << std::endl;
  std::cout << std::setfill(' ') << std::dec;  
  std::cout << "FW version : " << major_version << "."
	    << minor_version
	    << std::endl;

  std::cout << "\n#D: Mezzanine firmware" << std::endl;

  {
    uint32_t bct_version = ReadMIFModule(fpga_module, MsT::MIF::kDown,
					 HRTDC_MZN::BCT::kAddrVersion, 4);
    uint32_t major_version = (bct_version >> 8) & 0xff;
    uint32_t minor_version = (bct_version) & 0xff;

    std::cout << std::hex;
    std::cout << std::setfill('0') << std::right 
	      << "FW ID (MZN-D)     : 0x" << std::setw(4) << ((bct_version >> 16) & 0xffff) << std::endl;
    std::cout << std::setfill(' ') << std::dec;  
    std::cout << "FW version (MZN-D): " << major_version << "."
	      << minor_version
	      << std::endl;
  }

  return 0;

}// main
