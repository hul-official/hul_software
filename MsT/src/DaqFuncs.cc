#include<fstream>
#include<cstdlib>
#include<iostream>
#include<cstdio>
#include<csignal>

#include"DaqFuncs.hh"
#include"MifFunc.hh"
#include"RegisterMap.hh"
#include"UDPRBCP.hh"
#include"network.hh"
#include"errno.h"

int user_stop = 0;

static const int32_t kNumHead = 3;
static const int32_t kNumData = kNumHead + 2048;

static const int32_t kPrintStep = 100;

// signal -----------------------------------------------------------------
void
UserStop_FromCtrlC(int signal)
{
  std::cout << "Stop request" << std::endl;
  user_stop = 1;
}

// SetTdcWindowHRTDC ----------------------------------------------------
void
SetTdcWindowHRTDC(uint32_t wmax, uint32_t wmin, HUL::FPGAModule& fmodule, uint32_t addr_base)
{
  using namespace HUL;
  
  static const uint32_t kCounterMax  = 2047;
  static const uint32_t kPtrDiffWr   = 2;

  uint32_t ptr_ofs = kCounterMax - wmax + kPtrDiffWr;

  WriteMIFModule(fmodule, addr_base,
    		 HRTDC_MZN::TDC::kAddrPtrOfs, ptr_ofs, 2);
  WriteMIFModule(fmodule, addr_base,
    		 HRTDC_MZN::TDC::kAddrWinMax, wmax, 2);
  WriteMIFModule(fmodule, addr_base,
    		 HRTDC_MZN::TDC::kAddrWinMin, wmin, 2);
}

// SetTdcWindowLRTDC ---------------------------------------------------
void
SetTdcWindowLRTDC(uint32_t wmax, uint32_t wmin, HUL::FPGAModule& fpga_module)
{
  using namespace HUL::MsT;
  
  static const unsigned int c_max       = 2047;
  static const unsigned int ptr_diff_wr = 2;

  unsigned int ptr_ofs            = c_max - wmax + ptr_diff_wr;

  fpga_module.WriteModule(LRTDC::kAddrPtrOfs,  ptr_ofs, 2);
  fpga_module.WriteModule(LRTDC::kAddrWinMax, wmax,  2);
  fpga_module.WriteModule(LRTDC::kAddrWinMin, wmin,  2);
}


// DdrInitialize --------------------------------------------------------
void
DdrInitialize(HUL::FPGAModule& fmodule)
{
  using namespace HUL;
  using namespace HUL::MsT;
  
  std::cout << "#D : Do DDR initialize" << std::endl;
  // MZN
  {
    WriteMIFModule(fmodule, MIF::kDown,
		   HRTDC_MZN::DCT::kAddrTestMode, 1, 1 );
  }

  uint32_t reg = DCT::kRegTestMode;

  // Base
  fmodule.WriteModule(DCT::kAddrCtrlReg, reg);
  fmodule.WriteModule(DCT::kAddrInitDDR, 0);

  uint32_t ret = fmodule.ReadModule(DCT::kAddrRcvStatus, 1);

  {
    if( ret & DCT::kRegBitAligned){
      std::cout << "#D : DDR initialize succeeded (MZN-D)" << std::endl;
    }else{
      std::cout << "#E : Failed (MZN-D)" << std::endl;
      exit(-1);
    }
  }// bit aligned ?

  // Set DAQ mode
   
  {
    WriteMIFModule(fmodule, MIF::kDown,
		   HRTDC_MZN::DCT::kAddrTestMode, 0, 1 );
  }

  reg = 0;
  fmodule.WriteModule(DCT::kAddrCtrlReg, reg);
  
}// DdrInitialize

// CalibLUT ---------------------------------------------------------------
void
CalibLUT(HUL::FPGAModule& fmodule, uint32_t addr_base)
{
  using namespace HUL;
  using namespace HUL::MsT;
  
  WriteMIFModule(fmodule, addr_base, 
		 HRTDC_MZN::DCT::kAddrExtraPath, 1, 1);

  while(!(ReadMIFModule(fmodule, addr_base, HRTDC_MZN::TDC::kAddrStatus, 1) & HRTDC_MZN::TDC::kRegReadyLut)){
    sleep(1);
    std::cout << "#D waiting LUT ready" << std::endl;
  }// while

  std::cout << "#D LUT is ready! (MIF-D)" << std::endl;

  WriteMIFModule(fmodule, addr_base, 
		 HRTDC_MZN::DCT::kAddrExtraPath, 0, 1);
  WriteMIFModule(fmodule, addr_base, 
		 HRTDC_MZN::TDC::kAddrReqSwitch, 1, 1);
}

// execute daq ------------------------------------------------------------
void
DoDaq(std::string ip, int32_t runno, int32_t eventno)
{
  using namespace HUL;
  using namespace HUL::MsT;
  
  (void) signal(SIGINT, UserStop_FromCtrlC);

  // TCP socket
  int sock;
  if(-1 == (sock = ConnectSocket(ip))) return;
  std::cout << "#D: Socket connected" << std::endl;

  // UDP
  RBCP::UDPRBCP udp_rbcp(ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);     

  // Enable blocks
  uint32_t en_blocks = HRTDC_MZN::DCT::kEnLeading;
  WriteMIFModule(fpga_module, MIF::kDown,
		 HRTDC_MZN::DCT::kAddrEnBlocks, en_blocks, 1);

  // Set search windows
  // System clock of HR-TDC: 130MHz
  // System clock of LR-TDC: 150MHz
  // Then, window bins of HR-TDC and LR-TDC are 7.7 ns and 6.7 ns, respectively.
  uint32_t window_max_hrtdc         = 50;
  uint32_t window_min_hrtdc         = 0;
  SetTdcWindowHRTDC(window_max_hrtdc, window_min_hrtdc, fpga_module, MIF::kDown);
  
  uint32_t window_max_lrtdc         = 100;
  uint32_t window_min_lrtdc         = 0;
  SetTdcWindowLRTDC(window_max_lrtdc, window_min_lrtdc, fpga_module);

  // unset extra path
  //  fpga_module.WriteModule(DCT::kAddrExtraPath, 0);

  // evb reset
  fpga_module.WriteModule(DCT::kAddrResetEvb, 0);

  // set sel trig
  uint32_t sel_trig = TRM::kRegL1RM | TRM::kRegL2RM | TRM::kRegClrRM | TRM::kRegEnRM | TRM::kRegEnL2;
  fpga_module.WriteModule(TRM::kAddrSelectTrigger,  sel_trig, 2);

  //  uint32_t tdc_ctrl = HRTDC_MZN::TDC::kRegThrough | HRTDC_MZN::TDC::kRegStopDout;
  //  uint32_t tdc_ctrl = HRTDC_MZN::TDC::kRegStopDout;
  //  uint32_t tdc_ctrl = HRTDC_MZN::TDC::kRegAutosw;
  uint32_t tdc_ctrl = 0;
  WriteMIFModule(fpga_module, MIF::kDown,
		 HRTDC_MZN::TDC::kAddrControll, tdc_ctrl, 1);

  // Set MsT control register
  uint32_t reg_clear_prescale = 0;
  uint32_t reg_mst_timer      = 500;
  fpga_module.WriteModule(MST::kAddrClearPreset,  reg_clear_prescale, 1);
  fpga_module.WriteModule(MST::kAddrTimerPreset,  reg_mst_timer,      2);
  fpga_module.WriteModule(MST::kAddrBypass,       0,                  1);
  
  // Start DAQ
  WriteMIFModule(fpga_module, MIF::kDown,
		 HRTDC_MZN::DCT::kAddrGate, 1, 1);

  fpga_module.WriteModule(DCT::kAddrDaqGate,  1);

  std::string filename = "data/run" + std::to_string(runno) + ".dat";
  std::ofstream ofs(filename.c_str(), std::ios::binary);

  std::cout << "#D: Start DAQ" << std::endl;
  
  std::cout << "Start DAQ" << std::endl;
  // DAQ Cycle
  uint32_t buf[kNumData];
  for(int n = 0; n<eventno; ++n){
    int n_word;
    while( -1 == ( n_word = DoEventCycle(sock, buf)) && !user_stop) continue;
    if(user_stop) break;
    
    ofs.write((char*)buf, n_word*sizeof(uint32_t));

    if(n%kPrintStep==0){
      printf("\033[2J");
      printf("Event %d\n", n);
      for(int i = 0; i<n_word; ++i){
	printf("%8x ", buf[i]);
	if((i+1)%8 == 0) printf("\n");
      }// for(i)

      printf("\n");
    }

  }// For(n)

  fpga_module.WriteModule(DCT::kAddrDaqGate, 0);
  WriteMIFModule(fpga_module, MIF::kDown,
		 HRTDC_MZN::DCT::kAddrGate, 0, 1);  

  sleep(1);
  while(-1 != DoEventCycle(sock, buf));

  close(sock);
  ofs.close();
  
  return;
}

// ConnectSocket ----------------------------------------------------------
int
ConnectSocket(std::string ip)
{
  struct sockaddr_in SiTCP_ADDR;
  unsigned int port = 24;

  int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  SiTCP_ADDR.sin_family      = AF_INET;
  SiTCP_ADDR.sin_port        = htons((unsigned short int) port);
  SiTCP_ADDR.sin_addr.s_addr = inet_addr(ip.c_str());

  struct timeval tv;
  tv.tv_sec  = 3;
  tv.tv_usec = 0;
  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));

  int flag = 1;
  setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(flag));

  if(0 > connect(sock, (struct sockaddr*)&SiTCP_ADDR, sizeof(SiTCP_ADDR))){
    std::cerr << "#E : TCP connection error" << std::endl;
    close(sock);
    return -1;
  }
  
  return sock;
}

// Event Cycle ------------------------------------------------------------
int
DoEventCycle(int sock, uint32_t* buffer)
{
  // data read ---------------------------------------------------------
  static const unsigned int kSizeHeader = kNumHead*sizeof(unsigned int);
  int ret = Receive(sock, (char*)buffer, kSizeHeader);
  if(ret < 0) return -1;

  uint32_t n_word_data  = buffer[1] & 0x3ff;
  uint32_t size_data    = n_word_data*sizeof(unsigned int);
  
  if(n_word_data == 0) return kNumHead;

  ret = Receive(sock, (char*)(buffer + kNumHead), size_data);
  if(ret < 0) return -1;
  
  return kNumHead+ n_word_data;
}

// receive ----------------------------------------------------------------
int
Receive(int sock, char* data_buf, unsigned int length)
{
  unsigned int revd_size = 0;
  int tmp_ret            = 0;

  while(revd_size < length){
    tmp_ret = recv(sock, data_buf + revd_size, length -revd_size, 0);

    if(tmp_ret == 0) break;
    if(tmp_ret < 0){
      int errbuf = errno;
      perror("#D: TCP receive");
      if(errbuf == EAGAIN){
	// this is time out
      }else{
	// something wrong
	std::cerr << "#E: TCP error : " << errbuf << std::endl;
      }

      revd_size = tmp_ret;
      break;
    }

    revd_size += tmp_ret;
  }

  return revd_size;
}

