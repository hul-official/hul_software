#include"DaqFuncs.hh"

#include"UDPRBCP.hh"
#include"FPGAModule.hh"
#include"RegisterMap.hh"
#include"network.hh"
#include"errno.h"

#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<csignal>

int user_stop = 0;

static const int32_t kNumHead   = 3;
static const int32_t kNumBody   = 256;
static const int32_t kNumData   = kNumHead + kNumBody;

static const int32_t kPrintStep = 10;

// signal -----------------------------------------------------------------
void
UserStop_FromCtrlC(int signal)
{
  std::cout << "Stop request" << std::endl;
  user_stop = 1;
}

// execute daq ------------------------------------------------------------
void
DoDaq(std::string ip, int32_t runno, int32_t eventno)
{
  using namespace HUL::Scaler;
  
  (void) signal(SIGINT, UserStop_FromCtrlC);

  // TCP socket
  int sock;
  if(-1 == (sock = ConnectSocket(ip))) return;
  std::cout << "#D: Socket connected" << std::endl;

  // UDP
  RBCP::UDPRBCP udp_rbcp(ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);
  
    // set sel trig
  //  uint32_t reg_sel_trig = TRM::kRegL1Ext;
  uint32_t reg_sel_trig = TRM::kRegL1RM | TRM::kRegEnRM;
  //  uint32_t reg_sel_trig = TRM::kRegL1J0 | TRM::kRegL2J0 | TRM::kRegEnJ0 | TRM::kRegEnL2;
  fpga_module.WriteModule(TRM::kAddrSelectTrigger,  reg_sel_trig, 2);

  // Scaler block setting
  fpga_module.WriteModule(SCR::kAddrEnableBlock,   0xf);
  
  // Start DAQ
  fpga_module.WriteModule(SCR::kAddrCounterReset,  1);

  std::string filename = "data/run" + std::to_string(runno) + ".dat";
  std::ofstream ofs(filename.c_str(), std::ios::binary);

  if(!ofs.is_open()){
    std::cerr << "#E: Data file cannot be created.\n"
	      << "    Does a 'data' directory exist in the current directory?"
	      << std::endl;
    std::exit(-1);
  }

  std::cout << "#D: Open the DAQ gate and starts data aquisition." << std::endl;  
  fpga_module.WriteModule(DCT::kAddrDaqGate,  1);  

  // DAQ Cycle
  uint32_t buf[kNumData];
  for(int32_t n = 0; n<eventno; ++n){
    int32_t n_word;
    while( -1 == ( n_word = DoEventCycle(sock, buf)) && !user_stop) continue;
    if(user_stop) break;

    ofs.write((char*)buf, n_word*sizeof(unsigned int));

    if(n%kPrintStep==0){
      printf("\033[2J");
      printf("Event %d\n", n);
      for(int i = 0; i<n_word; ++i){
	printf("%8x ", buf[i]);
	if((i+1)%8 == 0) printf("\n");
      }// for(i)

      printf("\n");
    }

  }// For(n)

  std::cout << "#D: " << eventno << " events were accumulated." << std::endl;
  std::cout << "#D: Close the DAQ gate." << std::endl;  

  fpga_module.WriteModule(DCT::kAddrDaqGate,  0);

  std::cout << "#D: Read remaining data in FPGA. Wait time out." << std::endl;    
  sleep(1);
  while(-1 != DoEventCycle(sock, buf));
  std::cout << "#D: Time out is detected. End of DAQ." << std::endl;  

  //  shutdown(sock, SHUT_RDWR);
  close(sock);
  ofs.close();
  
  return;
}

// ConnectSocket ----------------------------------------------------------
int
ConnectSocket(std::string ip)
{
  struct sockaddr_in SiTCP_ADDR;
  unsigned int port = 24;

  int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  SiTCP_ADDR.sin_family      = AF_INET;
  SiTCP_ADDR.sin_port        = htons((unsigned short int) port);
  SiTCP_ADDR.sin_addr.s_addr = inet_addr(ip.c_str());

  struct timeval tv;
  tv.tv_sec  = 3;
  tv.tv_usec = 0;
  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));

  int flag = 1;
  setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(flag));

  if(0 > connect(sock, (struct sockaddr*)&SiTCP_ADDR, sizeof(SiTCP_ADDR))){
    std::cerr << "#E : TCP connection error" << std::endl;
    close(sock);
    return -1;
  }
  
  return sock;
}

// Event Cycle ------------------------------------------------------------
int
DoEventCycle(int sock, uint32_t* buffer)
{
  // data read ---------------------------------------------------------
  static const unsigned int kSizeHeader = kNumHead*sizeof(unsigned int);
  int ret = Receive(sock, (char*)buffer, kSizeHeader);
  if(ret < 0) return -1;

  uint32_t n_word_data  = buffer[1] & 0x3ff;
  uint32_t size_data    = n_word_data*sizeof(unsigned int);
  
  if(n_word_data == 0) return kNumHead;

  ret = Receive(sock, (char*)(buffer + kNumHead), size_data);
  if(ret < 0) return -1;
  
  return kNumHead+ n_word_data;
}

// receive ----------------------------------------------------------------
int
Receive(int sock, char* data_buf, unsigned int length)
{
  unsigned int revd_size = 0;
  int tmp_ret            = 0;

  while(revd_size < length){
    tmp_ret = recv(sock, data_buf + revd_size, length -revd_size, 0);

    if(tmp_ret == 0) break;
    if(tmp_ret < 0){
      int errbuf = errno;
      perror("#D: TCP receive");
      if(errbuf == EAGAIN){
	// this is time out
      }else{
	// something wrong
	std::cerr << "#E: TCP error : " << errbuf << std::endl;
      }

      revd_size = tmp_ret;
      break;
    }

    revd_size += tmp_ret;
  }

  return revd_size;
}

