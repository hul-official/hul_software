#include <iostream>
#include <cstdio>
#include <fstream>
#include <sstream>
#include <string>

#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "FPGAModule.hh"
#include "rbcp.hh"
#include "MtxConfig.hh"

#define DEBUG 0

using namespace HUL;

uint32_t registers[MTX3D_E03E42::kLengthMtxReg] = {0};

int  read_matrix_file(std::string& filename);
void send_matrix(HUL::FPGAModule& fpga_module);

// Main -----------------------------------------------------------------------------------
int
main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "load_matrix3d [IP address] [matrix_file]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];
  std::string file_name = argv[2];
  
  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);
  
  if(-1 == read_matrix_file(file_name)) return -1;
  send_matrix(fpga_module);
  
  std::cout << "#D : The matrix file (" << file_name << ") is successfully sent" << std::endl;
  std::cout << "Mtx3D: " << file_name << std::endl;

  return 0;

}// main

// read_matrix_file -----------------------------------------------------------------------
int
read_matrix_file(std::string& filename)
{
  std::ifstream ifs(filename.c_str());
  if(!ifs.is_open()){
    std::cout << "#E : No such the matrix file" << std::endl;
    return -1;
  }

  int32_t index = 0;
  std::string line;
  while(getline(ifs, line)){
    // this is comment line
    if(line[0] == '#' || line.empty()) continue;
    
    std::istringstream LineToWord(line);
    std::string word;
    // skip SCH#
    LineToWord >> word;
    LineToWord >> word; // this is target

    registers[index] = 0;
    for(int i = 0; i<MTX3D_E03E42::kNumBh2; ++i){
      uint32_t bit = (word[i] == '0' ? 0 : 1);
      bit = bit << i;
      registers[index] += bit;
    }// for(i)

#if DEBUG
    printf("0x%0x\n", registers[index]);
#endif
    
    ++index;
  }// while

  return 0;
}// read_matrix_file

// send_matrix --------------------------------------------------------------------
void
send_matrix(HUL::FPGAModule& fpga_module)
{
  static const unsigned int mask = 0xff;

  for(int i = MTX3D_E03E42::kLengthMtxReg-1; i>-1; --i){
    unsigned int reg = registers[i]  & mask;
    fpga_module.WriteModule(MTX3D_E03E42::MTX3D::kAddrEnableMtx,  reg, 1);
    fpga_module.WriteModule(MTX3D_E03E42::MTX3D::kAddrExecSR, 0);
  }// for(i)
}// send_matrix
