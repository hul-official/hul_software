#ifndef MTXFUNCS
#define MTXFUNCS

#include<stdint.h>
#include<string>

namespace HUL{
  class FPGAModule;
};

void    SetDWG(HUL::FPGAModule& fpga_module, uint32_t local_address,
	       int32_t delay, int32_t width);

#endif
