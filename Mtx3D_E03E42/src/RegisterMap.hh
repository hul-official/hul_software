#ifndef REGISTER_HH
#define REGISTER_HH

#include<stdint.h>
#include"RegisterMapCommon.hh"

namespace HUL{
namespace MTX3D_E03E42{
//-------------------------------------------------------------------------
// Matrix2D-1 Module
//-------------------------------------------------------------------------
namespace MTX2D1{
  enum LocalAddress{
    kAddrTofDWG    = 0x00000000, // W/R, [63:0] (8 byte)
    kAddrTofExtDWG = 0x00100000, // W/R, [63:0] (8 byte)
    kAddrSchDWG    = 0x00200000, // W/R, [63:0] (8 byte)
    kAddrTrigDWG   = 0x00300000, // W/R, [63:0] (8 byte)
    kAddrEnableMtx = 0x00400000, // W/,  [0:0]
    kAddrExecSR    = 0x01000000  // W/,  Execute MtxRegSR
  };
};

//-------------------------------------------------------------------------
// Matrix2D-2 Module
//-------------------------------------------------------------------------
namespace MTX2D2{
  enum LocalAddress{
    kAddrTofDWG    = 0x10000000, // W/R, [63:0] (8 byte)
    kAddrTofExtDWG = 0x10100000, // W/R, [63:0] (8 byte)
    kAddrSchDWG    = 0x10200000, // W/R, [63:0] (8 byte)
    kAddrTrigDWG   = 0x10300000, // W/R, [63:0] (8 byte)
    kAddrEnableMtx = 0x10400000, // W/,  [0:0]
    kAddrExecSR    = 0x11000000  // W/,  Execute MtxRegSR
  };
};

//-------------------------------------------------------------------------
// Matrix3D Module
//-------------------------------------------------------------------------
namespace MTX3D{
  enum LocalAddress{
    kAddrTofDWG    = 0x20000000, // W/R, [63:0] (8 byte)
    kAddrTofExtDWG = 0x20100000, // W/R, [63:0] (8 byte)
    kAddrSchDWG    = 0x20200000, // W/R, [63:0] (8 byte)
    kAddrBh2DWG    = 0x20300000, // W/R, [63:0] (8 byte)
    kAddrTrigDWG   = 0x20400000, // W/R, [63:0] (8 byte)
    kAddrEnableMtx = 0x20500000, // W/,  [7:0]
    kAddrExecSR    = 0x21000000  // W/,  Execute MtxRegSR
  };
};


//-------------------------------------------------------------------------
// IOM Module
//-------------------------------------------------------------------------
namespace IOM{
  enum LocalAddress{
    kAddrNimout1     = 0x30000000, // W/R, [3:0]
    kAddrNimout2     = 0x30100000, // W/R, [3:0]
    kAddrNimout3     = 0x30200000, // W/R, [3:0]
    kAddrNimout4     = 0x30300000  // W/R, [3:0]
  };

  enum OutputSubbAddress{
    kReg_o_Trig3D      = 0x0,
    kReg_o_Trig2Dveto1 = 0x1,
    kReg_o_Trig2Dveto2 = 0x2,
    kReg_o_Trig2D1     = 0x3,
    kReg_o_Trig2D2     = 0x4,
    kReg_o_TofOR       = 0x5,
    kReg_o_SchOR       = 0x6,
    kReg_o_Bh2OR       = 0x7
  };

};
};
};
#endif
