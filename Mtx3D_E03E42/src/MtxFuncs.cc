#include"MtxFuncs.hh"

#include"UDPRBCP.hh"
#include"FPGAModule.hh"
#include"RegisterMap.hh"
#include"rbcp.hh"
#include"network.hh"
#include"errno.h"

#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<bitset>

// DWG --------------------------------------------------------------
void
SetDWG(HUL::FPGAModule& fpga_module, uint32_t local_address,
       int32_t delay, int32_t width)
{
  const int32_t kMaxDelay = 31;
  const int32_t kMaxWidth = 32;
  
  if(delay > kMaxDelay){
    std::cerr << "#E: DWG delay requirement is exceeded 31."
	      << "    Address: 0x" << std::hex << local_address << std::dec
	      << "    Delay:     " << delay
	      << std::endl;
    std::cerr << "    Delay value of 31 is forced to set"
	      << std::endl;

    delay = kMaxDelay;
  }

  if(width > kMaxWidth){
    std::cerr << "#E: DWG width requirement is exceeded 32."
	      << "    Address: 0x" << std::hex << local_address << std::dec
	      << "    Width:     " << width
	      << std::endl;
    std::cerr << "    Width value of 32 is forced to set"
	      << std::endl;

    width = kMaxWidth;
  }

  if(width == 0){
    std::cerr << "#E: DWG width 0 is now allowed."
	      << "    Address: 0x" << std::hex << local_address << std::dec
	      << "    Width:     " << width
	      << std::endl;
    std::cerr << "    Width value of 1 is forced to set"
	      << std::endl;

    width = 1;
  }
  
  // Make 64-bit vector
  const int32_t kLengthSR = 64;

  int32_t delay_count = delay;
  int32_t width_count = width;
  
  std::bitset<kLengthSR> bits;
  bits.reset();
  for(int32_t i = kLengthSR-1; i>=0; --i){
    if(delay_count != 0){
      --delay_count;
      continue;
    }

    if(width_count != 0){
      bits.set(i);
      --width_count;
    }
  }

  // Send 8 byte register
  const int32_t kNumByte = 8;
  uint64_t reg64 = bits.to_ullong();
  fpga_module.WriteModule64(local_address,  reg64, kNumByte);

  return;
}
