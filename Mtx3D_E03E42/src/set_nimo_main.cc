#include <iostream>
#include <iomanip>
#include <ios>
#include <cstdio>

#include "RegisterMap.hh"
#include "network.hh"
#include "FPGAModule.hh"
#include "UDPRBCP.hh"
#include "MtxFuncs.hh"
#include "rbcp.hh"

using namespace HUL;

int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "set_nimo [IP address] [signal] [NimoOut port]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];
  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);

  std::string sel_out = argv[2];
  int reg = stoi(sel_out);

  std::string sel_nim = argv[3];
  int nimport = stoi(sel_nim);

  if(nimport == 1){
    fpga_module.WriteModule(MTX3D_E03E42::IOM::kAddrNimout1, reg);
  }else if(nimport == 2){
    fpga_module.WriteModule(MTX3D_E03E42::IOM::kAddrNimout2, reg);
  }else if(nimport == 3){
    fpga_module.WriteModule(MTX3D_E03E42::IOM::kAddrNimout3, reg);
  }else if(nimport == 4){
    fpga_module.WriteModule(MTX3D_E03E42::IOM::kAddrNimout4, reg);  
  }else{
    std::cout << "#E : No such port" << std::endl;
  }

  return 0;

}// main
