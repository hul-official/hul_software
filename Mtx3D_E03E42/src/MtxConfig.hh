#ifndef MTX_CONFIG_HH
#define MTX_CONFIG_HH

namespace HUL{
namespace MTX3D_E03E42{
  const int32_t kNumTof = 28;
  const int32_t kNumSch = 64;
  const int32_t kNumBh2 = 8;

  const int32_t kLengthMtxReg = kNumTof*kNumSch;
  
};
};

#endif
