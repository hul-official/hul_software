#include <iostream>
#include <cstdio>
#include <fstream>
#include <sstream>
#include <string>

#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "FPGAModule.hh"
#include "rbcp.hh"
#include "MtxConfig.hh"

#define DEBUG 1

using namespace HUL;
uint32_t registers[MTX3D_E03E42::kLengthMtxReg] = {0};

int  read_matrix_file(std::string& filename);
void send_matrix(HUL::FPGAModule& fpga_module, uint32_t address_offset);

// Main -----------------------------------------------------------------------------
int
main(int argc, char* argv[])
{
  if(1 == argc || argc != 4){
    std::cout << "Usage\n";
    std::cout << "load_matrix2d [IP address] [matrix_file] [Mtx2D num (1 or 2)]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip  = argv[1];
  std::string file_name = argv[2];
  std::string mtx2d_num = argv[3];
  const int32_t address_offset = mtx2d_num[0] == '1' ? 0 : 0x10000000;

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);
  
  if(-1 == read_matrix_file(file_name)) return -1;
  send_matrix(fpga_module, address_offset);
  
  std::cout << "#D : The matrix file (" << file_name << ") is successfully sent" << std::endl;
  std::cout << "Mtx2D: " << file_name << std::endl;

  return 0;

}// main

// read_matrix_file ----------------------------------------------------------------
int
read_matrix_file(std::string& filename)
{
  std::ifstream ifs(filename.c_str());
  if(!ifs.is_open()){
    std::cout << "#E : No such the matrix file" << std::endl;
    return -1;
  }

  int32_t index = 0;
  std::string line;
  while(getline(ifs, line)){
    // this is comment line
    if(line[0] == '#' || line.empty()) continue;
    
    std::istringstream LineToWord(line);
    std::string word;
    // skip SCH#
    LineToWord >> word;
    LineToWord >> word; // this is target
    //    std::cout << word << std::endl;

    registers[index] = 0;
    uint32_t bit = (word[0] == '0' ? 0 : 1);
    registers[index] = bit;
    //    std::cout << std::hex << registers_low[index] << " " << registers_high[index]<< std::endl;
    
    ++index;
  }// while

  return 0;
}// read_matrix_file

// send_matrix ---------------------------------------------------------------------
void
send_matrix(HUL::FPGAModule& fpga_module, uint32_t address_offset)
{
  static const uint32_t mask = 0x1;

  for(int i = MTX3D_E03E42::kLengthMtxReg-1; i>-1; --i){
    uint32_t reg = registers[i]  & mask;
    fpga_module.WriteModule(MTX3D_E03E42::MTX2D1::kAddrEnableMtx + address_offset,  reg);
    fpga_module.WriteModule(MTX3D_E03E42::MTX2D1::kAddrExecSR + address_offset, 0);
    //    std::cout << std::hex << reg_high << " " << reg_low<< std::endl;
  }// for(i)
}// send_matrix
