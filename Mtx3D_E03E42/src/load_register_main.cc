#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <ios>
#include <cstdio>
#include <map>

#include "RegisterMap.hh"
#include "network.hh"
#include "FPGAModule.hh"
#include "UDPRBCP.hh"
#include "MtxFuncs.hh"
#include "rbcp.hh"

#define DEBUG 1

namespace HUL{
  namespace MTX3D_E03E42{
  struct Register{
    uint32_t local_address;
    int32_t  delay;
    int32_t  width;
  };

  using DetectorMap = std::map<std::string, Register>;
  using ModuleMap   = std::map<std::string, DetectorMap>;

  ModuleMap g_register_map =
    {
     {"MTX2D1", {
		{"Tof",    {MTX2D1::kAddrTofDWG,    0, 1}},
		{"TofExt", {MTX2D1::kAddrTofExtDWG, 0, 1}},
		{"Sch",    {MTX2D1::kAddrSchDWG,    0, 1}},
		{"Trig",   {MTX2D1::kAddrTrigDWG,   0, 1}}
       }
     },
     {"MTX2D2", {
		 {"Tof",    {MTX2D2::kAddrTofDWG,    0, 1}},
		 {"TofExt", {MTX2D2::kAddrTofExtDWG, 0, 1}},
		 {"Sch",    {MTX2D2::kAddrSchDWG,    0, 1}},
		 {"Trig",   {MTX2D2::kAddrTrigDWG,   0, 1}}
       }
     },
     {"MTX3D", {
		{"Tof",    {MTX3D::kAddrTofDWG,    0, 1}},
		{"TofExt", {MTX3D::kAddrTofExtDWG, 0, 1}},
		{"Sch",    {MTX3D::kAddrSchDWG,    0, 1}},
		{"Bh2",    {MTX3D::kAddrBh2DWG,    0, 1}},
		{"Trig",   {MTX3D::kAddrTrigDWG,   0, 1}}
       }
     }
    };
  };};

int  read_register_file(std::string& filename);
void send_register(HUL::FPGAModule& fpga_module);

// Main -----------------------------------------------------------------------
int
main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "load_register [IP address] [register_file]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip  = argv[1];
  std::string file_name = argv[2];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);
  if(-1 == read_register_file(file_name)) return -1;
  send_register(fpga_module);

  return 0;

}// main


// read_register_file --------------------------------------------------

int
read_register_file(std::string& filename)
{
  std::ifstream ifs(filename.c_str());
  if(!ifs.is_open()){
    std::cout << "#E : No such the register file" << std::endl;
    return -1;
  }

  std::string line;
  while(getline(ifs, line)){
    // this is comment line
    if(line[0] == '#' || line.empty()) continue;
    
    std::istringstream LineToWord(line);
    std::string word, key;
    int32_t     reg;
    LineToWord >> word;
    LineToWord >> reg;
    key = word;

    int32_t index_sep = word.find_first_of("::");
    std::string module_name = word.substr(0, index_sep);
    word = word.substr(index_sep+2, word.size() - index_sep -2);

    index_sep = word.find_first_of("::");
    std::string detector_name = word.substr(0, index_sep);
    std::string reg_name = word.substr(index_sep+2, word.size() - index_sep -2);

    HUL::MTX3D_E03E42::Register& cont
      = HUL::MTX3D_E03E42::g_register_map[module_name][detector_name];
    if(reg_name == "Delay") cont.delay = reg;
    if(reg_name == "Width") cont.width = reg;

#if DEBUG
    printf("%-25s, Reg: %4d\n", key.c_str(), reg);
#endif


  }
  return 0;
}

// send_register
void
send_register(HUL::FPGAModule& fpga_module)
{
  for(const auto& first_pair : HUL::MTX3D_E03E42::g_register_map){
    for(const auto& second_pair : first_pair.second){
      HUL::MTX3D_E03E42::Register cont = second_pair.second;
#if DEBUG
      printf("Address: 0x%08x, Delay: %4d, Width: %4d\n",
	     cont.local_address, cont.delay, cont.width);
#endif

      SetDWG(fpga_module, cont.local_address, cont.delay, cont.width);
    }
  }
}
