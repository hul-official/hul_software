#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <ios>
#include <cstdio>
#include <map>

#include "RegisterMap.hh"
#include "network.hh"
#include "FPGAModule.hh"
#include "UDPRBCP.hh"
#include "MtxFuncs.hh"
#include "rbcp.hh"

#define DEBUG 1

int  load_register_file(std::string& filename, HUL::FPGAModule& fpga_module);

int
main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "load_nimo [IP address] [register_file]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip  = argv[1];
  std::string file_name = argv[2];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);
  if(-1 == load_register_file(file_name, fpga_module)) return -1;

  return 0;

}// main


// read_register_file --------------------------------------------------
namespace HUL{
  namespace MTX3D_E03E42{
  struct Register{
    uint32_t local_address;
    int32_t  reg;
  };

  using NimMap = std::map<std::string, Register>;

  NimMap g_register_map =
    {
     {"OUT1", {IOM::kAddrNimout1,  0}},
     {"OUT2", {IOM::kAddrNimout2,  0}},
     {"OUT3", {IOM::kAddrNimout3,  0}},
     {"OUT4", {IOM::kAddrNimout4,  0}}
    };
  };};

int
load_register_file(std::string& filename, HUL::FPGAModule& fpga_module)
{
  std::ifstream ifs(filename.c_str());
  if(!ifs.is_open()){
    std::cout << "#E : No such the register file" << std::endl;
    return -1;
  }

  std::string line;
  while(getline(ifs, line)){
    // this is comment line
    if(line[0] == '#' || line.empty()) continue;
    
    std::istringstream LineToWord(line);
    std::string key;
    int32_t     reg;
    LineToWord >> key;
    LineToWord >> reg;

    HUL::MTX3D_E03E42::Register& cont
      = HUL::MTX3D_E03E42::g_register_map[key];
    cont.reg = reg;

#if DEBUG
    printf("%-10s, Address: 0x%08x, Command: %d\n",
	   key.c_str(), cont.local_address, cont.reg);
#endif

    fpga_module.WriteModule(cont.local_address, cont.reg);
  }
  return 0;
}

