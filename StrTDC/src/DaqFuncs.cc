#include"DaqFuncs.hh"

#include"UDPRBCP.hh"
#include"FPGAModule.hh"
#include"RegisterMap.hh"
#include"network.hh"
#include"errno.h"

#include<iostream>
#include<fstream>
#include<cstdio>
#include<cstdlib>
#include<csignal>
#include<list>

int user_stop = 0;

static const int32_t kNumData   = 16384*10;
static const int32_t kNumByte   = 5;
struct DataCont{
  uint8_t data[kNumData*kNumByte];
  int32_t n_word;
};

static const int32_t kPrintStep = 100;

// signal -----------------------------------------------------------------
void
UserStop_FromCtrlC(int signal)
{
  std::cout << "Stop request" << std::endl;
  user_stop = 1;
}

// execute daq ------------------------------------------------------------
void
DoDaq(std::string ip, int32_t runno, int32_t num_spill)
{
  using namespace HUL::STRTDC;
  
  (void) signal(SIGINT, UserStop_FromCtrlC);

  // TCP socket
  int sock;
  if(-1 == (sock = ConnectSocket(ip))) return;
  std::cout << "#D: Socket connected" << std::endl;

  // UDP
  RBCP::UDPRBCP udp_rbcp(ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);

  fpga_module.WriteModule(ODP::kAddrEnFilter,       0 );
  fpga_module.WriteModule(ODP::kAddrMinTh,          0 );
  fpga_module.WriteModule(ODP::kAddrMaxTh,          0 );
  fpga_module.WriteModule(ODP::kAddrEnZeroThrough,  0 );
  fpga_module.WriteModule(ODP::kAddrTwCorr0,        0 );
  fpga_module.WriteModule(ODP::kAddrTwCorr1,        0 );
  fpga_module.WriteModule(ODP::kAddrTwCorr2,        0 );
  fpga_module.WriteModule(ODP::kAddrTwCorr3,        0 );
  fpga_module.WriteModule(ODP::kAddrTwCorr4,        0 );

  // Start DAQ
  fpga_module.WriteModule(DCT::kAddrDaqGate,       1);

  std::string filename = "data/run" + std::to_string(runno) + ".dat";
  std::ofstream ofs(filename.c_str(), std::ios::binary);
  int32_t n_spill = 0;

  std::cout << "#D: Start DAQ" << std::endl;
  // DAQ Cycle
  std::list<DataCont> data_list;
  for(;;){
    DataCont dcont = {{0},0};
    bool flag_run_end = false;

    while( -1 == ( dcont.n_word = DoEventCycle(sock, dcont.data)) && !user_stop) continue;

    if(dcont.data[0] != 0){
      if(dcont.n_word == -4){
	for(int i = 0; i<kNumData; ++i){
#if DEBUG
	  printf("%02x%02x%02x%02x%02x ", 
		 dcont.data[kNumByte*i+4], 
		 dcont.data[kNumByte*i+3], 
		 dcont.data[kNumByte*i+2], 
		 dcont.data[kNumByte*i+1],
		 dcont.data[kNumByte*i+0]
		 );
	  if((i+1)%12 == 0) printf("\n");
#endif
	  if((dcont.data[kNumByte*i+4] & 0xff) == 0x40){
	    dcont.n_word = i+1;
	    data_list.push_back(dcont);
	    printf("\n#D: Spill End is detected\n");
	    ++n_spill;
	  }

	  if(n_spill == num_spill){
	    printf("\n#D: Run end\n");
	    flag_run_end = true;
	    break;
	  }
	}// for(i)
      }else{
	printf("Recording...");
	fflush(stdout);
	data_list.push_back(dcont);
      }//spill end ?

    }// event record
    
    if(flag_run_end) break;
  }// For(n)

  fpga_module.WriteModule(DCT::kAddrDaqGate,  0);

  std::list<DataCont>::iterator itr     = data_list.begin();
  std::list<DataCont>::iterator itr_end = data_list.end();
  for(; itr != itr_end; ++itr){
    //    std::cout << "N:" << (*itr).n_word << std::endl;
    ofs.write((char*)(*itr).data, (*itr).n_word*sizeof(uint8_t)*kNumByte);
  }
  
  //  shutdown(sock, SHUT_RDWR);
  close(sock);
  ofs.close();
  
  return;
}

// ConnectSocket ----------------------------------------------------------
int
ConnectSocket(std::string ip)
{
  struct sockaddr_in SiTCP_ADDR;
  unsigned int port = 24;

  int sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  SiTCP_ADDR.sin_family      = AF_INET;
  SiTCP_ADDR.sin_port        = htons((unsigned short int) port);
  SiTCP_ADDR.sin_addr.s_addr = inet_addr(ip.c_str());

  struct timeval tv;
  tv.tv_sec  = 3;
  tv.tv_usec = 0;
  setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv, sizeof(tv));

  int flag = 1;
  setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, (char*)&flag, sizeof(flag));

  if(0 > connect(sock, (struct sockaddr*)&SiTCP_ADDR, sizeof(SiTCP_ADDR))){
    std::cerr << "#E : TCP connection error" << std::endl;
    close(sock);
    return -1;
  }
  
  return sock;
}

// Event Cycle ------------------------------------------------------------
int
DoEventCycle(int sock, uint8_t* buffer)
{
  // data read ---------------------------------------------------------
  static const unsigned int kSizeHeader = kNumByte*kNumData*sizeof(uint8_t);
  int ret = Receive(sock, (char*)buffer, kSizeHeader);
  if(ret < 0) return ret;

  return kNumData;
}

// receive ----------------------------------------------------------------
int
Receive(int sock, char* data_buf, unsigned int length)
{
  unsigned int revd_size = 0;
  int tmp_ret            = 0;

  while(revd_size < length){
    tmp_ret = recv(sock, data_buf + revd_size, length -revd_size, 0);

    if(tmp_ret == 0) break;
    if(tmp_ret < 0){
      int errbuf = errno;
      perror("#D: TCP receive");
      if(errbuf == EAGAIN){
	// this is time out
      }else{
	// something wrong
	std::cerr << "#E: TCP error : " << errbuf << std::endl;
      }

      revd_size = tmp_ret;
      break;
    }

    revd_size += tmp_ret;
  }

  return revd_size;
}

