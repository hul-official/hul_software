#ifndef REGISTERH_HH
#define REGISTERH_HH

namespace HUL{
namespace STRTDC{
//-------------------------------------------------------------------------
// DCT Module
//-------------------------------------------------------------------------
namespace DCT{
  enum RegisterAddress{
    kAddrDaqGate       = 0x00000000  // W/R, [0:0] Set DAQ Gate reg
  };
};
  
//-------------------------------------------------------------------------
// ODP Module
//-------------------------------------------------------------------------
namespace ODP{
  enum RegisterAddress{
    kAddrEnFilter      = 0x10000000, // W/R, [0:0] Enable/Disable TOT filter
    kAddrMinTh         = 0x10100000, // W,R, [7:0] TOT filter Min. Th.
    kAddrMaxTh         = 0x10200000, // W,R, [7:0] TOT filter Max. Th.
    kAddrEnZeroThrough = 0x10300000, // W,R, [0:0] TOT filter zero through enable.
    kAddrTwCorr0       = 0x10400000, // W,R, [7:0] Timewalk corr. 
    kAddrTwCorr1       = 0x10500000, // W,R, [7:0] Timewalk corr. 
    kAddrTwCorr2       = 0x10600000, // W,R, [7:0] Timewalk corr. 
    kAddrTwCorr3       = 0x10700000, // W,R, [7:0] Timewalk corr. 
    kAddrTwCorr4       = 0x10800000  // W,R, [7:0] Timewalk corr. 
  };
};

};
};
#endif
