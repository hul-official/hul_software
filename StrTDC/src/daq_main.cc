#include <iostream>
#include <cstdio>

#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "FPGAModule.hh"
#include "DaqFuncs.hh"

using namespace HUL::STRTDC;
enum argIndex{kBin, kIp, kRunNo, kNumSpill};

int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "daq [IP address] [RunNo.] [# of spill]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[kIp];
  int32_t runno      = atoi(argv[kRunNo]);
  int32_t numspill   = atoi(argv[kNumSpill]);

  std::cout << "#D: Entering data streaming mode." << std::endl;
  
  DoDaq(board_ip, runno, numspill);

  return 0;

}// main
