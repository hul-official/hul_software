#include <iostream>
#include <cstdio>

#include "RegisterMapCommon.hh"
#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "FPGAModule.hh"
#include "BctBusBridgeFunc.hh"
#include "rbcp.hh"

using namespace HUL;
void read_a_mzn(HUL::FPGAModule& fpga_module, uint32_t mif_id);

int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "read_mzn_sem [IP address]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);

  printf("==== Mezzanine slot Up ====\n");
  if(HRTDC_BASE::kEnSlotUp) read_a_mzn(fpga_module, HRTDC_BASE::BBP::kUpper);
  else                      printf("Slot is disabled.\n");

  printf("\n");

  printf("==== Mezzanine slot Down ====\n");
  if(HRTDC_BASE::kEnSlotDown) read_a_mzn(fpga_module, HRTDC_BASE::BBP::kLower);
  else                        printf("Slot is disabled.\n");

  return 0;

}// main

// read_a_mzn ____________________________________________________________________________
void
read_a_mzn(HUL::FPGAModule& fpga_module, uint32_t base_addr)
{
  // SEM ______________________________________________________________

  const uint32_t sem_correction_count
    = ReadModuleIn2ndryFPGA(fpga_module, base_addr, HRTDC_MZN::SDS::kAddrSemCorCount, 2);

  const uint32_t status
    = ReadModuleIn2ndryFPGA(fpga_module, base_addr, HRTDC_MZN::SDS::kAddrSdsStatus, 1);

  printf("Num. correction. : %d \n", sem_correction_count);
  printf("\n");

  printf("SEM error status (0:nomarl, 1:error)\n");
  printf(" - Sem watchdog      : %d\n", (status & SDS::kSemWatchdogAlarm) >> 4);
  printf(" - Sem uncorrectable : %d\n", (status & SDS::kSemUncorrectableAlarm) >> 5);
}
