#include <iostream>
#include <cstdio>

#include "RegisterMapCommon.hh"
#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "FPGAModule.hh"
#include "BctBusBridgeFunc.hh"
#include "rbcp.hh"

using namespace HUL;
void read_a_mzn(HUL::FPGAModule& fpga_module, uint32_t base_addr);

int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "read_mzn_xadc [IP address]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);

  printf("==== Mezzanine slot Up ====\n");
  if(HRTDC_BASE::kEnSlotUp) read_a_mzn(fpga_module, HRTDC_BASE::BBP::kUpper);
  else                      printf("Slot is disabled.\n");

  printf("\n");

  printf("==== Mezzanine slot Down ====\n");
  if(HRTDC_BASE::kEnSlotDown) read_a_mzn(fpga_module, HRTDC_BASE::BBP::kLower);
  else                        printf("Slot is disabled.\n");

  return 0;

}// main

// read_a_mzn ____________________________________________________________________________
void
read_a_mzn(HUL::FPGAModule& fpga_module, uint32_t base_addr)
{
  // XADC _____________________________________________________________
  WriteModuleIn2ndryFPGA(fpga_module, base_addr,
			 HRTDC_MZN::SDS::kAddrXadcDrpMode, SDS::kDrpReadMode, 1);

  // Read temperature monitor
  WriteModuleIn2ndryFPGA(fpga_module, base_addr,
			 HRTDC_MZN::SDS::kAddrXadcDrpAddr, SDS::kAddrDrpTemp, 1);
  WriteModuleIn2ndryFPGA(fpga_module, base_addr,
			 HRTDC_MZN::SDS::kAddrXadcExecute, 1, 1);
  uint32_t adc_temp = ReadModuleIn2ndryFPGA(fpga_module, base_addr,
					    HRTDC_MZN::SDS::kAddrXadcDrpDout, 2);

#if 0
  // Read VCCINT
  WriteModuleIn2ndryFPGA(fpga_module, base_addr,
			 HRTDC_MZN::SDS::kAddrXadcDrpAddr, SDS::kAddrDrpVccInt, 1);
  WriteModuleIn2ndryFPGA(fpga_module, base_addr,
			 HRTDC_MZN::SDS::kAddrXadcExecute, 1, 1);
  uint32_t adc_vccint = ReadModuleIn2ndryFPGA(fpga_module, base_addr,
					      HRTDC_MZN::SDS::kAddrXadcDrpDout, 2);

  // Read VCCAUX
  WriteModuleIn2ndryFPGA(fpga_module, base_addr,
			 HRTDC_MZN::SDS::kAddrXadcDrpAddr, SDS::kAddrDrpVccAux, 1);
  WriteModuleIn2ndryFPGA(fpga_module, base_addr,
			 HRTDC_MZN::SDS::kAddrXadcExecute, 1, 1);
  uint32_t adc_vccaux = ReadModuleIn2ndryFPGA(fpga_module, base_addr,
					      HRTDC_MZN::SDS::kAddrXadcDrpDout, 2);
  
#endif
  // Read status
  uint32_t status = ReadModuleIn2ndryFPGA(fpga_module, base_addr,
					  HRTDC_MZN::SDS::kAddrSdsStatus, 1);
  
  // translate
  const uint32_t shift   = 4;
  //  const uint32_t mask    = 0xfff;
  const uint32_t max_adc = 0x1000;

  double temp = (adc_temp >> shift)*503.975/max_adc - 273.15; // 503.975: magic number

  printf("FPGA temp.  : %.2f C\n", temp);
  printf("\n");

  // Status
  printf("XADC status (0:false, 1:true)\n");
  printf(" - Over temp.   (Th. 125C)   : %d\n", (status & SDS::kXadcOverTemperature) >> 0);
  printf(" - Temp alarm   (Th. 85C)    : %d\n", (status & SDS::kXadcUserTempAlarm)   >> 1);

}
