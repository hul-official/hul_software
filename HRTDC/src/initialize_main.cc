#include <iostream>
#include <iomanip>
#include <ios>
#include <cstdio>

#include "RegisterMap.hh"
#include "network.hh"
#include "FPGAModule.hh"
#include "UDPRBCP.hh"
#include "BctBusBridgeFunc.hh"
#include "rbcp.hh"
#include "DaqFuncs.hh"

using namespace HUL;
int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "initialize [IP address]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);
  fpga_module.WriteModule(BCT::kAddrReset, 0);

  if(HRTDC_BASE::kEnSlotUp){
    uint32_t reg = fpga_module.WriteModule(HRTDC_BASE::DCT::kAddrCtrlReg, 1);
    fpga_module.WriteModule(HRTDC_BASE::DCT::kAddrCtrlReg, reg | HRTDC_BASE::DCT::kRegFRstU);
    fpga_module.WriteModule(HRTDC_BASE::DCT::kAddrCtrlReg, reg);
  }

  if(HRTDC_BASE::kEnSlotDown){
    uint32_t reg = fpga_module.WriteModule(HRTDC_BASE::DCT::kAddrCtrlReg, 1);
    fpga_module.WriteModule(HRTDC_BASE::DCT::kAddrCtrlReg, reg | HRTDC_BASE::DCT::kRegFRstD);
    fpga_module.WriteModule(HRTDC_BASE::DCT::kAddrCtrlReg, reg);
  }
  
  DdrInitialize(fpga_module);
  if(HRTDC_BASE::kEnSlotUp)   CalibLUT(fpga_module, HRTDC_BASE::BBP::kUpper);
  if(HRTDC_BASE::kEnSlotDown) CalibLUT(fpga_module, HRTDC_BASE::BBP::kLower);

  return 0;

}// main
