#include <iostream>
#include <cstdio>

#include "RegisterMapCommon.hh"
#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "FPGAModule.hh"
#include "BctBusBridgeFunc.hh"
#include "rbcp.hh"

using namespace HUL;
void set_error_injection_address(HUL::FPGAModule& fpga_module, uint32_t base_addr,
				 uint32_t command,
				 uint32_t addr_lniear_frame,
				 uint32_t addr_word,
				 uint32_t addr_bit
				 );

int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "read_mzn_sem [IP address]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);

  printf("==== Mezzanine slot Up ====\n");
  if(HRTDC_BASE::kEnSlotUp){
    printf("Inject SEU\n");
    set_error_injection_address(fpga_module, HRTDC_BASE::BBP::kUpper,
				SDS::kSetIdle, 0, 0, 0);
    WriteModuleIn2ndryFPGA(fpga_module, HRTDC_BASE::BBP::kUpper, HRTDC_MZN::SDS::kAddrSemErrStrobe, 1, 1);

    set_error_injection_address(fpga_module, HRTDC_BASE::BBP::kUpper,
				SDS::kSetError, 100, 10, 1);
    WriteModuleIn2ndryFPGA(fpga_module, HRTDC_BASE::BBP::kUpper, HRTDC_MZN::SDS::kAddrSemErrStrobe, 1, 1);

    set_error_injection_address(fpga_module, HRTDC_BASE::BBP::kUpper,
				SDS::kSetObserve, 0, 0, 0);
    WriteModuleIn2ndryFPGA(fpga_module, HRTDC_BASE::BBP::kUpper, HRTDC_MZN::SDS::kAddrSemErrStrobe, 1, 1);
  }else{
    printf("Slot is disabled.\n");
  }

  printf("\n");

  printf("==== Mezzanine slot Down ====\n");
  if(HRTDC_BASE::kEnSlotDown){
    printf("Inject SEU\n");
    set_error_injection_address(fpga_module, HRTDC_BASE::BBP::kLower,
				SDS::kSetIdle, 0, 0, 0);
    WriteModuleIn2ndryFPGA(fpga_module, HRTDC_BASE::BBP::kLower, HRTDC_MZN::SDS::kAddrSemErrStrobe, 1, 1);

    set_error_injection_address(fpga_module, HRTDC_BASE::BBP::kLower,
				SDS::kSetError, 100, 10, 1);
    WriteModuleIn2ndryFPGA(fpga_module, HRTDC_BASE::BBP::kLower, HRTDC_MZN::SDS::kAddrSemErrStrobe, 1, 1);

    set_error_injection_address(fpga_module, HRTDC_BASE::BBP::kLower,
				SDS::kSetObserve, 0, 0, 0);
    WriteModuleIn2ndryFPGA(fpga_module, HRTDC_BASE::BBP::kLower, HRTDC_MZN::SDS::kAddrSemErrStrobe, 1, 1);

  }else{
    printf("Slot is disabled.\n");
  }

  return 0;

}// main

// control sem in mzn ___________________________________________________________________
void
set_error_injection_address(HUL::FPGAModule& fpga_module, uint32_t base_addr,
			    uint32_t command,
			    uint32_t addr_linear_frame,
			    uint32_t addr_word,
			    uint32_t addr_bit
			    )
{
  // command              :  4bit
  // linear frame address : 17bit
  // word address         :  7bit
  // bit address          :  5bit
  const uint32_t mask_linear = 0x1ffff;
  const uint32_t mask_word   = 0x7f;
  const uint32_t mask_bit    = 0x1f;

  const uint32_t shift_word   = 5;
  const uint32_t shift_linear = 7 + shift_word;

  // For command (5th byte)
  const uint32_t mask_command  = 0xf;
  const uint32_t shift_command = 4;

  uint32_t reg_1st_4th = addr_bit & mask_bit;
  reg_1st_4th = reg_1st_4th | ((addr_word & mask_word) << shift_word);
  reg_1st_4th = reg_1st_4th | ((addr_linear_frame & mask_linear) << shift_linear);
  WriteModuleIn2ndryFPGA(fpga_module, base_addr,
			 HRTDC_MZN::SDS::kAddrSemErrAddr, reg_1st_4th, 4);

  uint32_t reg_5th = (command & mask_command) << shift_command;
  WriteModuleIn2ndryFPGA(fpga_module, base_addr,
			 HRTDC_MZN::SDS::kAddrSemErrAddr+4, reg_5th, 1);
}
