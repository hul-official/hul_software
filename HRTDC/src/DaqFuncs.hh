#ifndef DAQFUNCS_HH
#define DAQFUNCS_HH

#include<string>
#include<stdint.h>

#include"FPGAModule.hh"

void    SetTdcWindow(uint32_t wmax, uint32_t wmin, HUL::FPGAModule& fmodule, uint32_t base_addr);
void    DdrInitialize(HUL::FPGAModule& fmodule);
void    CalibLUT(HUL::FPGAModule& fmodule, uint32_t base_addr);

void    UserStop_FromCtrlC(int signal);
void    DoDaq(std::string ip, int32_t runno, int32_t eventno);

int32_t ConnectSocket(std::string ip);
int32_t DoEventCycle(int sock, uint32_t* buffer);

int32_t Receive(int sock, char* data_buf, unsigned int length);

#endif
