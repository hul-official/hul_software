#include <iostream>
#include <cstdio>

#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "FPGAModule.hh"
#include "rbcp.hh"

using namespace HUL::HRTDC_BASE;
int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "set_nimio [IP address]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[1];

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);
  //  fpga_module.WriteModule(IOM::kAddrExtL1, IOM::kReg_i_Nimin1);
  fpga_module.WriteModule(IOM::kAddrNimout1, 13);
  fpga_module.WriteModule(IOM::kAddrNimout2, 14);
  fpga_module.WriteModule(IOM::kAddrNimout3, 15);
  fpga_module.WriteModule(IOM::kAddrNimout4, IOM::kReg_o_clk1kHz);
  //  fpga_module.WriteModule(IOM::kAddrNimout4, IOM::kReg_o_clk1MHz);

  return 0;

}// main
