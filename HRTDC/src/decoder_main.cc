#include<iostream>
#include<fstream>
#include<string>
#include<ios>
#include<stdint.h>

#include"TH1.h"
#include"TFile.h"
#include"TTree.h"
#include"TROOT.h"

static const int32_t kMaxHit = 64*16;
static const int32_t kNumCh  = 64;

// 0-31ch (Slot-U), 32-63ch (Slot-D)
enum slot{kUp, kDown, kNumSlot};
int32_t stop_out[kNumSlot];
int32_t through[kNumSlot];
int32_t over_flow[kNumSlot];

// Common stop
int32_t stop_fine_count[kNumSlot];
int32_t stop_estimator[kNumSlot];
int32_t stop_coarse_count[kNumSlot];
int32_t common_stop[kNumSlot];

// Leading
int32_t num_hit; // total # of multi-hit in HUL
int32_t num_hit_ch[kNumCh]; // # of multi-hit in Ch
int32_t channel[kMaxHit];
int32_t fine_count[kMaxHit];
int32_t estimator[kMaxHit];
int32_t coarse_count[kMaxHit];
int32_t tdc_leading[kMaxHit];

// Trailing
int32_t tnum_hit;
int32_t tnum_hit_ch[kNumCh];
int32_t tchannel[kMaxHit];
int32_t tfine_count[kMaxHit];
int32_t testimator[kMaxHit];
int32_t tcoarse_count[kMaxHit];
int32_t tdc_trailing[kMaxHit];

int32_t raw_tdc[kMaxHit];

enum IndexArgv{
  kBin, kRunNo, kNumArg
};

int main(int argc, char* argv[])
{
  if(argc == 1){
    std::cout << "#D: Usage\n";
    std::cout << " decoder [RunNo.]" << std::endl;
    return 0;
  }
  
  // File open
  std::string run_number   = argv[kRunNo];
  std::string infile_path  = "data/run" + run_number + ".dat";
  std::string outfile_path = "rootfile/run" + run_number + ".root";

  std::ifstream ifs(infile_path.c_str(), std::ios::binary);
  if(!ifs.is_open()){
    std::cerr << "#E: No such file " << infile_path << std::endl;
    return -1;
  }

  //  h_Utility::igzfilter ifs(ifs_org);

  std::cout << "#D: Start decode, Run No. " << run_number << std::endl;

  // ROOT file create
  TFile fout(outfile_path.c_str(), "recreate");
  if(!fout.IsOpen()){
    std::cerr << "#E: A root file cannot be created in " << outfile_path << "\n"
	      << "    Does a 'rootfile' directory exist in the current directory?"
	      << std::endl;
    std::exit(-1);
  }
  TTree tree("tree", "tree");
  tree.Branch("through",              through,         "through[2]/I");
  tree.Branch("stop_out",             stop_out,        "stop_out[2]/I");
  tree.Branch("over_flow",            over_flow,       "over_flow[2]/I");

  tree.Branch("stop_fine_count",      stop_fine_count, "stop_fine_count[2]/I");
  tree.Branch("stop_estimator",       stop_estimator,  "stop_estimator[2]/I");
  tree.Branch("stop_coarse_count",    stop_coarse_count,"stop_coarse_count[2]/I");
  tree.Branch("common_stop",          common_stop,     "common_stop[2]/I");

  tree.Branch("num_hit",             &num_hit,         "num_hit/I");
  tree.Branch("num_hit_ch",           num_hit_ch,      "num_hit_ch[64]/I");
  tree.Branch("channel",              channel,         "channel[num_hit]/I");
  tree.Branch("fine_count",           fine_count,      "fine_count[num_hit]/I");
  tree.Branch("estimator",            estimator,       "estimator[num_hit]/I");
  tree.Branch("coarse_count",         coarse_count,    "coarse_count[num_hit]/I");
  tree.Branch("tdc_leading",          tdc_leading,     "tdc_leading[num_hit]/I");

  tree.Branch("tnum_hit",            &tnum_hit,        "tnum_hit/I");
  tree.Branch("tnum_hit_ch",          tnum_hit_ch,     "tnum_hit_ch[32]/I");
  tree.Branch("tchannel",             tchannel,        "tchannel[tnum_hit]/I");
  tree.Branch("tfine_count",          tfine_count,     "tfine_count[tnum_hit]/I");
  tree.Branch("testimator",           testimator,      "testimator[tnum_hit]/I");
  tree.Branch("tcoarse_count",        tcoarse_count,   "tcoarse_count[tnum_hit]/I");
  tree.Branch("tdc_trailing",         tdc_trailing,    "tdc_trailing[num_hit]/I");

  // decode
  static const uint32_t kMagic          = 0xffff80eb;
  static const uint32_t kMagicSlotU     = 0xfa000000;
  static const uint32_t kMagicSlotD     = 0xfb000000;
  static const uint32_t kMagicLeading   = 0x6;
  static const uint32_t kMagicTrailing  = 0x5;
  static const uint32_t kMagicStop      = 0x4;

  static const uint32_t kMaskSlot       = 0xff000000;

  static const int32_t kShiftDataHead   = 29;
  static const int32_t kMaskDataHead    = 7;

  static const int32_t kShiftThrough    = 12;
  static const int32_t kShiftStopDout   = 13;
  static const int32_t kShiftOverflow   = 14;
  static const int32_t kMaskNumWord     = 0x7ff;

  static const int32_t kShiftCh     = 24;
  static const int32_t kMaskCh      = 0x1f;

  static const int32_t kShiftCoarse = 11;
  static const int32_t kMaskCoarse  = 0x1fff;

  static const int32_t kMaskFine    = 0x7ff;

  static const int32_t kMaskTdc     = 0xffffff;

  uint32_t buf[2048];
  uint32_t header1;
  uint32_t header2;
  uint32_t header3;
  int32_t  n_of_word;
  int32_t  index_slot=0;
  
  while(!ifs.eof()){
    ifs.read((char*)&header1, sizeof(uint32_t ));
    
    if(header1 == kMagic){
      num_hit       = 0;
      tnum_hit      = 0;
      index_slot = 0;

      // header
      ifs.read((char*)&header2, sizeof(uint32_t ));
      ifs.read((char*)&header3, sizeof(uint32_t ));
      n_of_word = header2 & kMaskNumWord;

      // data body
      ifs.read((char*)buf, n_of_word*sizeof(uint32_t ));
      int32_t n_read_word = ifs.gcount()/(int32_t)sizeof(uint32_t);
      //      std::cout << "read " << n_read_word << std::endl;
      //      std::cout << "head " << n_of_word << std::endl;
      if(n_read_word != n_of_word){
	if(!ifs.eof()){
	  // not eof
	  std::cerr << "#E: read count mis-match" << std::endl;
	}else{
	  break;
	}

      }// error

      for(int32_t i = 0; i<kNumCh; ++i){
	num_hit_ch[i]  = 0;
	tnum_hit_ch[i] = 0;
      }// for(i)

      for(int32_t i = 0; i<n_of_word; ++i){
	// slot identification
	uint32_t reg_slot = (buf[i] & kMaskSlot);
	if(kMagicSlotU == reg_slot || kMagicSlotD == reg_slot){
	  index_slot = kMagicSlotU == reg_slot ? 0 : 1;
	  over_flow[index_slot] = (buf[i] >> kShiftOverflow) & 0x1;
	  stop_out[index_slot]  = (buf[i] >> kShiftStopDout) & 0x1;
	  through[index_slot]   = (buf[i] >> kShiftThrough) & 0x1;
	}

	// Data body
	uint32_t data_header = (buf[i] >> kShiftDataHead) & kMaskDataHead;
	if(data_header == kMagicLeading){
	  tdc_leading[num_hit] = buf[i] & kMaskTdc;

	  channel[num_hit]          = ((buf[i] >> kShiftCh)  & kMaskCh) + index_slot*32;
	  uint32_t fc   = (buf[i])              & kMaskFine;
	  fine_count[num_hit]   = through[index_slot] == 1 ? fc : -1;
	  estimator[num_hit]    = through[index_slot] == 0 ? fc : -1;
	  coarse_count[num_hit] = (buf[i] >> kShiftCoarse)  & kMaskCoarse;

	  num_hit_ch[channel[num_hit]]++;
	  num_hit++;
	}// leading data

	if(data_header == kMagicTrailing){
	  tdc_trailing[tnum_hit] = buf[i] & kMaskTdc;

	  tchannel[tnum_hit] = ((buf[i] >> kShiftCh)  & kMaskCh) + index_slot*32;
	  uint32_t fc    = (buf[i])              & kMaskFine;
	  tfine_count[tnum_hit]   = through[index_slot] == 1 ? fc : -1;
	  testimator[tnum_hit]    = through[index_slot] == 0 ? fc : -1;
	  tcoarse_count[tnum_hit] = (buf[i] >> kShiftCoarse)  & kMaskCoarse;

	  tnum_hit_ch[tchannel[tnum_hit]]++;
	  tnum_hit++;
	}// trailing data

	if(data_header == kMagicStop){
	  common_stop[index_slot]    = buf[i] & kMaskTdc;

	  uint32_t fc= (buf[i])              & kMaskFine;
	  stop_fine_count[index_slot]   = through[index_slot] == 1 ? fc : -1;
	  stop_estimator[index_slot]    = through[index_slot] == 0 ? fc : -1;
	  stop_coarse_count[index_slot] = (buf[i] >> kShiftCoarse)  & kMaskCoarse;
	}// common stop data

      }// for(i)

      tree.Fill();
    }// decode one event
  }// while

  fout.Write();
  fout.Close();

  std::cout << "#D: End of decode" << std::endl;

  return 0;
}
