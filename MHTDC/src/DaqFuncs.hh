#ifndef DAQFUNCS
#define DAQFUNCS

#include<stdint.h>
#include<string>

namespace HUL{
  class FPGAModule;
};

void    UserStop_FromCtrlC(int signal);
void    DoDaq(std::string ip, int32_t runno, int32_t eventno);

void    SetTdcWindow(uint32_t wmax, uint32_t wmin, HUL::FPGAModule& fpga_module);

int32_t ConnectSocket(std::string ip);
int32_t DoEventCycle(int sock, uint32_t* buffer);

int32_t Receive(int sock, char* data_buf, unsigned int length);

#endif
