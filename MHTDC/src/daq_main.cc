#include <iostream>
#include <cstdio>

#include "RegisterMap.hh"
#include "network.hh"
#include "UDPRBCP.hh"
#include "FPGAModule.hh"
#include "DaqFuncs.hh"

enum argIndex{kBin, kIp, kRunNo, kEventNo};
using namespace HUL::MHTDC;
int main(int argc, char* argv[])
{
  if(1 == argc){
    std::cout << "Usage\n";
    std::cout << "daq [IP address] [RunNo.] [# of Events]" << std::endl;
    return 0;
  }// usage
  
  // body ------------------------------------------------------
  std::string board_ip = argv[kIp];
  int32_t runno      = atoi(argv[kRunNo]);
  int32_t eventno    = atoi(argv[kEventNo]);

  RBCP::UDPRBCP udp_rbcp(board_ip, RBCP::gUdpPort, RBCP::UDPRBCP::kNoDisp);
  HUL::FPGAModule fpga_module(udp_rbcp);
  std::cout << "hoge" << std::endl;
  fpga_module.WriteModule(DCT::kAddrResetEvb, 1);

  DoDaq(board_ip, runno, eventno);

  return 0;

}// main
